# convr

## Installation

Install runs a script that installs this package's peerDeps as the apps own 
devDependencies (flat). Manual config is currently needed.

``` bash
$ npm i -D @td7x/convr
```

> On the first run you will still receive the unmet peer peerDependencies 
warning because the install script is not run as preinstall due to its own deps.

## Usage

.lintstagedrc
```json
{
  "src/**/*": [
    "tslint --fix",
    "prettier --write",
    "git add"
  ],
  ".gitlab-ci.yml": [
    "gitlab-ci-lint"
  ]
}
```

.prettierrc
```json
{
  "trailingComma": "es5",
  "listDifferent": true
}
```

package.json
```json
  "scripts": {
    "release": "git push --follow-tags origin master && conventional-gitlab-releaser -p angular",
    "commitmsg": "commitlint --extends @commitlint/config-angular -e",
    "precommit": "lint-staged",
    "prepush": "npm run test:unit"
  }
```
