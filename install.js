const fs = require("fs");
const path = require("path");
const shell = require("shelljs");
const url = require("url");

const home = require("@td7x/home-court").path;

let args = ["install", "--no-save", "--silent"];
let installer = "npm";
const pkg = JSON.parse(
  fs.readFileSync(path.resolve(__dirname, "package.json"), "utf8")
);

let repo =
  typeof pkg.repository === "string"
    ? pkg.repository
    : url.parse(pkg.repository.url || " ").hostname;

if (repo.includes("gitlab")) {
  delete pkg.optionalDependencies["conventional-github-releaser"];
} else if (repo.includes("github")) {
  delete pkg.optionalDependencies["conventional-gitlab-releaser"];
  delete pkg.optionalDependencies["gitlab-ci-lint"];
}

let packages = Object.keys(pkg.optionalDependencies).map(function(key) {
  return `${key}@${pkg.optionalDependencies[key]}`;
});

const hasYarn = fs.existsSync(path.resolve(home, "yarn.lock"));
if (hasYarn) {
  installer = "yarn";
  args = ["add", "--silent"];
}

args = args.concat(packages);

console.log(`\n
${pkg.name} installing to ${home}:\n${installer} ${args.join(" ")}
`);
console.log("this might take a bit ...");

shell.cd(home);
shell.exec(`${installer} ${args.join(" ")}`);

console.log(`
Put the release tools to work by configuring lint-stage, husky, and run scripts.
Look to https://gitlab.com/td7x/convr for an example.
`);
