# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.11.0"></a>
# [0.11.0](https://gitlab.com/td7x/convr/compare/v0.10.0...v0.11.0) (2017-10-26)


### Features

* **core:** simplier workflow with publish rather than release ([4f06a72](https://gitlab.com/td7x/convr/commit/4f06a72))



<a name="0.10.0"></a>
# [0.10.0](https://gitlab.com/td7x/convr/compare/v0.9.2...v0.10.0) (2017-10-26)


### Features

* **core:** remove commit prompt to simplify ([45f74d9](https://gitlab.com/td7x/convr/commit/45f74d9))



<a name="0.9.2"></a>
## [0.9.2](https://gitlab.com/td7x/convr/compare/v0.9.1...v0.9.2) (2017-10-26)


### Bug Fixes

* **ci:** include devdeps for linting ([5c28587](https://gitlab.com/td7x/convr/commit/5c28587))



<a name="0.9.1"></a>
## [0.9.1](https://gitlab.com/td7x/convr/compare/v0.9.0...v0.9.1) (2017-07-21)


### Bug Fixes

* **core:** update deps versions ([f9f348b](https://gitlab.com/td7x/convr/commit/f9f348b))



<a name="0.9.0"></a>
# [0.9.0](https://gitlab.com/td7x/convr/compare/v0.8.2...v0.9.0) (2017-07-21)


### Features

* **core:** avoid modifing consuming package.json ([6440381](https://gitlab.com/td7x/convr/commit/6440381))



<a name="0.8.2"></a>
## [0.8.2](https://gitlab.com/td7x/convr/compare/v0.8.2-2...v0.8.2) (2017-07-18)



<a name="0.8.2-2"></a>
## [0.8.2-2](https://gitlab.com/td7x/convr/compare/v0.8.2-1...v0.8.2-2) (2017-07-18)



<a name="0.8.2-1"></a>
## [0.8.2-1](https://gitlab.com/td7x/convr/compare/v0.8.2-0...v0.8.2-1) (2017-07-18)



<a name="0.8.2-0"></a>
## [0.8.2-0](https://gitlab.com/td7x/convr/compare/v0.8.1...v0.8.2-0) (2017-07-18)



<a name="0.8.1"></a>
## [0.8.1](https://gitlab.com/td7x/convr/compare/v0.8.0-3...v0.8.1) (2017-07-18)



<a name="0.8.0"></a>
# [0.8.0](https://gitlab.com/td7x/convr/compare/v0.8.0-3...v0.8.0) (2017-07-18)



<a name="0.8.0-3"></a>
# [0.8.0-3](https://gitlab.com/td7x/convr/compare/v0.8.0...v0.8.0-3) (2017-07-18)



<a name="0.8.0-2"></a>
# [0.8.0-2](https://gitlab.com/td7x/convr/compare/v0.8.0-1...v0.8.0-2) (2017-07-17)


### Features

* **core:** optionalDeps vs peerDeps ([7c8621f](https://gitlab.com/td7x/convr/commit/7c8621f))



<a name="0.8.0-1"></a>
# [0.8.0-1](https://gitlab.com/td7x/convr/compare/v0.8.0-0...v0.8.0-1) (2017-07-17)



<a name="0.8.0-0"></a>
# [0.8.0-0](https://gitlab.com/td7x/convr/compare/v0.7.0...v0.8.0-0) (2017-07-17)


### Features

* **core:** rm home-court deps ([eca1ac7](https://gitlab.com/td7x/convr/commit/eca1ac7))



<a name=""></a>
# [](https://gitlab.com/td7x/convr/compare/v0.7.0...v) (2017-07-17)


### Features

* **core:** rm home-court deps ([eca1ac7](https://gitlab.com/td7x/convr/commit/eca1ac7))



<a name="0.7.0"></a>
# [0.7.0](https://gitlab.com/td7x/convr/compare/v0.6.0...v0.7.0) (2017-07-16)


### Features

* **core:** update deps ([9aeb7c6](https://gitlab.com/td7x/convr/commit/9aeb7c6))



<a name="0.6.0"></a>
# [0.6.0](https://gitlab.com/td7x/convr/compare/v0.5.1...v0.6.0) (2017-07-16)


### Features

* **core:** reduce deps ([59fa976](https://gitlab.com/td7x/convr/commit/59fa976))
* **core:** reduce deps ([2a2489e](https://gitlab.com/td7x/convr/commit/2a2489e))



<a name="0.5.1"></a>
## [0.5.1](https://gitlab.com/td7x/convr/compare/v0.5.0...v0.5.1) (2017-07-16)


### Bug Fixes

* **core:** flatten due to no transpile ([a1f83a5](https://gitlab.com/td7x/convr/commit/a1f83a5))



<a name="0.5.0"></a>
# [0.5.0](https://gitlab.com/td7x/convr/compare/v0.4.0...v0.5.0) (2017-07-16)


### Features

* **ci:** dogfooding ([3416399](https://gitlab.com/td7x/convr/commit/3416399))
* **core:** explicitly installs peerDeps ([e29559a](https://gitlab.com/td7x/convr/commit/e29559a))



<a name="0.4.0"></a>
# [0.4.0](https://gitlab.com/td7x/convr/compare/v0.3.1...v0.4.0) (2017-07-12)


### Features

* **core:** update deps. add lint-staged ([44c8c02](https://gitlab.com/td7x/convr/commit/44c8c02))



<a name="0.3.1"></a>
## [0.3.1](https://gitlab.com/td7x/convr/compare/v0.3.0...v0.3.1) (2017-06-19)



<a name="0.3.0"></a>
# [0.3.0](https://gitlab.com/tom.davidson/convr/compare/v0.1.1...v0.3.0) (2017-06-19)


### Features

* **core:** new gitlab-lint-ci ([9ab81ef](https://gitlab.com/tom.davidson/convr/commit/9ab81ef))



<a name="0.1.1"></a>
## [0.1.1](https://gitlab.com/tom.davidson/convr/compare/v0.1.0-beta.1...v0.1.1) (2017-06-19)


### Bug Fixes

* **install:** disable automatic run script change on install ([380ebe1](https://gitlab.com/tom.davidson/convr/commit/380ebe1))



<a name="0.1.0"></a>
# [0.1.0](https://gitlab.com/tom.davidson/convr/compare/v0.1.0-beta.1...v0.1.0) (2017-06-19)


### Bug Fixes

* **install:** disable automatic run script change on install ([380ebe1](https://gitlab.com/tom.davidson/convr/commit/380ebe1))



<a name="0.1.0-beta.1"></a>
# [0.1.0-beta.1](https://gitlab.com/tom.davidson/convr/compare/v0.1.0...v0.1.0-beta.1) (2017-05-07)



<a name="0.1.0-alpha.0"></a>
# [0.1.0-alpha.0](https://gitlab.com/tom.davidson/convr/compare/0.1.0-alpha.0...v0.1.0-alpha.0) (2017-05-07)



<a name="0.1.0-beta.0"></a>
# [0.1.0-beta.0](https://gitlab.com/tom.davidson/convr/compare/v0.1.0...v0.1.0-beta.0) (2017-05-07)



<a name="0.1.0-alpha.0"></a>
# [0.1.0-alpha.0](https://gitlab.com/tom.davidson/convr/compare/v0.0.2...v0.1.0-alpha.0) (2017-05-07)



<a name="0.0.2"></a>
## 0.0.2 (2017-05-07)


### Bug Fixes

* **core:** normalized package.json ([c9a93c7](https://gitlab.com/tom.davidson/convr/commit/c9a93c7))
